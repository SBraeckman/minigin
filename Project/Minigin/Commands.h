#include "Actor.h"
#include <memory>

class Command
{
public:
	virtual ~Command() {};
	virtual void Execute(std::weak_ptr<Actor> actor) = 0;
};

class FireCommand : public Command
{
public:
	virtual void Execute(std::weak_ptr<Actor> actor) override
	{
		actor.lock()->Fire();
	}
};

class UpCommand : public Command
{
public:
	virtual void Execute(std::weak_ptr<Actor> actor) override
	{
		actor.lock()->Move(Direction::Up);
	}
};

class LeftCommand : public Command
{
public:
	virtual void Execute(std::weak_ptr<Actor> actor) override
	{
		actor.lock()->Move(Direction::Left);
	}
};

class RightCommand : public Command
{
public:
	virtual void Execute(std::weak_ptr<Actor> actor) override
	{
		actor.lock()->Move(Direction::Right);
	}
};

class DownCommand : public Command
{
public:
	virtual void Execute(std::weak_ptr<Actor> actor) override
	{
		actor.lock()->Move(Direction::Down);
	}
};