#pragma once

namespace Physics
{
	class Rigidbody;
	class Collider;
	class PhysicsHandler
	{
	public:

		void HandlePhysics(float elapsedTime);
		void AddRigidbody(std::weak_ptr<Rigidbody> rb);

	private:

		Vector3 m_Gravity{ 0, 0, 0 };
		std::vector<std::weak_ptr<Rigidbody>> m_pRigidBodies;
		std::vector<std::weak_ptr<Rigidbody>> m_pDynamicRigidBodies;
		std::vector<std::weak_ptr<Rigidbody>> m_pStaticRigidBodies;

		void CollideAndResolve(std::weak_ptr<Collider> dyn, std::weak_ptr<Collider> stat, std::weak_ptr<Rigidbody> dynRigidbody, std::weak_ptr<Rigidbody> statRigidbody);
	};
}

