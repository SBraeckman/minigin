#pragma once
#include "Singleton.h"
#include <unordered_map>
namespace dae
{
	class Texture2D;
}
class TextureManager final : public dae::Singleton<TextureManager>
{
public:
	void AddTexture(const std::string& path, const std::string& name);
	void AddTexture(std::shared_ptr<dae::Texture2D> texture, const std::string& name);
	std::weak_ptr<dae::Texture2D> GetTexture(const std::string& name);

private:
	friend class dae::Singleton<TextureManager>;
	TextureManager() = default;
	std::unordered_map<std::string, std::shared_ptr<dae::Texture2D>> m_Textures{};
};

