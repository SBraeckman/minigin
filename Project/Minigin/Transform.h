#pragma once

namespace dae
{
	class Transform final
	{
	public:
		const Vector3& GetPosition() const { return m_Position; }
		void SetPosition(float x, float y, float z);
		void SetPosition(const Vector3& position);
		void SetPosition(const Vector2& position);
	private:
		Vector3 m_Position;
	};
}
