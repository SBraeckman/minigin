#pragma once
#include <memory>

class GameObject;
namespace Physics
{
	class Collider;
}

class CollisionObserver
{
public:
	virtual ~CollisionObserver() = default;
	virtual void OnTriggerEnter(std::weak_ptr<Physics::Collider> other) = 0;
};
