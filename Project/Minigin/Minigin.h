#pragma once
struct SDL_Window;
namespace dae
{
	class Minigin
	{
	public:
		void Initialize();
		virtual void LoadGame() const {};
		void Cleanup();
		void Run();

		static Vector2 g_WindowSize;
		static float g_DeltaTime;

	private:
		static const int MsPerFrame = 16; //16 for 60 fps, 33 for 30 fps
		SDL_Window* m_Window{};
	};
}