#include "MiniginPCH.h"
#include "Scene.h"
#include "InputManager.h"
#include "Actor.h"
#include "Rigidbody.h"

using namespace dae;

unsigned int Scene::m_IdCounter = 0;

Scene::Scene(const std::string& name) : m_Name(name) {}

void dae::Scene::HandleInputs()
{
	if (!m_pControlledActor.lock())
		return;
	auto& input = InputManager::GetInstance();
	std::shared_ptr<Command> command = input.HandleInput();
	do
	{
		if(command)
			command->Execute(m_pControlledActor);
		command = input.HandleInput();
	} while (command);
	command = input.HandleContinuousInput();
	if(command)
		command->Execute(m_pControlledActor);
}

Scene::~Scene() = default;

void Scene::Add(const std::shared_ptr<SceneObject>& object)
{
	m_Objects.push_back(object);
	object->SetScenePtr(object);
	std::shared_ptr<GameObject> go = std::dynamic_pointer_cast<GameObject>(object);
	if (go)
	{
		std::weak_ptr<Physics::Rigidbody> rb{go->GetComponent<Physics::Rigidbody>()};
		if (rb.lock())
		{
			m_PhysicsHandler.AddRigidbody(rb);
		}
	}
}

void Scene::Update(float elapsedTime)
{
	HandleInputs();

	for (int i{}; i < (int)m_Objects.size(); i++)
	{
		if (m_Objects[i]->IsEnabled())
			m_Objects[i]->Update();
	}

	m_PhysicsHandler.HandlePhysics(elapsedTime);

	for (int i{}; i < (int)m_Objects.size(); i++)
	{
		if (m_Objects[i]->IsEnabled())
			m_Objects[i]->LateUpdate();
	}
}

void Scene::Render() const
{
	for (const auto& object : m_Objects)
	{
		if(object->IsEnabled())
			object->Render();
	}
}

void dae::Scene::SetControlledActor(std::weak_ptr<Actor> actor)
{
	m_pControlledActor = actor;
}

Physics::PhysicsHandler& dae::Scene::GetPhysics()
{
	return m_PhysicsHandler;
}

std::shared_ptr<SceneObject> dae::Scene::GetObjectWithTag(const std::string& tag)
{
	for (auto go : m_Objects)
	{
		if (go->GetTag() == tag)
		{
			return go;
		}
	}
	return std::shared_ptr<SceneObject>();
}


