#pragma once
#include "Component.h"
#include "Subjects.h"

namespace Physics
{
	class ColliderKey
	{
	public:
		void DoSomething() {};
	private:
		friend class PhysicsHandler;
		ColliderKey(int code = 0) { code = 1; };
		
		ColliderKey(const ColliderKey& other) = delete;
		ColliderKey(ColliderKey&& other) = delete;
		ColliderKey& operator=(const ColliderKey& other) = delete;
		ColliderKey& operator=(ColliderKey&& other) = delete;
	};

	enum class ColliderType
	{
		Rectangle
	};

	struct ColliderInfo
	{
		Vector3 center{};
		ColliderType type{};
		bool isPlatform{};
		float width{};
		float height{};
	};

	class Collider :  public Component, public CollisionSubject
	{
	public:

		void Update() = 0;
		void Render() const = 0;

		virtual const ColliderInfo& GetInfo() const;

		void SetPlatform(bool value);
		void SetPosition(const Vector3& center);
		void SetPosition(float x, float y, float z);
		void SetTrigger(bool value);
		bool IsTrigger() const;

		virtual void OnAdd() override;

		void CollisionHappened(std::weak_ptr<Collider> otherCollider, ColliderKey ck);

		Collider() = default;
		virtual ~Collider() = default;
		Collider(const Collider& other) = delete;
		Collider(Collider&& other) = delete;
		Collider& operator=(const Collider& other) = delete;
		Collider& operator=(Collider&& other) = delete;

	protected:

		void SetWidth(float width)
		{
			m_Info.width = width;
		}

		void SetHeight(float height)
		{
			m_Info.height = height;
		}

		void SetType(ColliderType type)
		{
			m_Info.type = type;
		}

	private:

		ColliderInfo m_Info;
		bool m_IsTrigger{ false };
	};


}

