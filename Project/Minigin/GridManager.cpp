#include "MiniginPCH.h"
#include "GridManager.h"

const Vector2 GridManager::g_TileDimensions{ 20,18 };

void GridManager::LoadNewGrid(GridTile* grid)
{
	m_GridTiles = grid;
}

bool GridManager::IsValidMovement(const Vector2& position, Direction direction, bool canDig)
{
	std::pair<int,int> tileIndex = TileIndexFromPosition(position);

	if (tileIndex.first + tileIndex.second >= 0 && tileIndex.first + tileIndex.second < m_GridHeight * m_GridWidth)
	{
		switch (direction)
		{
		case Direction::Left:
			if (tileIndex.first == 0)
				return false;

			tileIndex.first--;
			if (!IsTileOpenInDirection((Direction)-(int)direction, GetTile(tileIndex)))
			{
				if (canDig)
				{
					OpenTileInDirection((Direction)-(int)direction, GetTile(tileIndex));
					tileIndex.first++;
					OpenTileInDirection(direction, GetTile(tileIndex));
				}
				else
					return false;
			}
			break;
		case Direction::Right:
			if (tileIndex.first >= m_GridWidth - 1)
				return false;

			tileIndex.first++;
			if (!IsTileOpenInDirection((Direction)-(int)direction, GetTile(tileIndex)))
			{
				if (canDig)
				{
					OpenTileInDirection((Direction)-(int)direction, GetTile(tileIndex));
					tileIndex.first--;
					OpenTileInDirection(direction, GetTile(tileIndex));
				}
				else
					return false;
			}
			break;
		case Direction::Up:
			if (tileIndex.second >= m_GridHeight - 1)
				return false;

			tileIndex.second++;
			if (!IsTileOpenInDirection((Direction)-(int)direction, GetTile(tileIndex)))
			{
				if (canDig)
				{
					OpenTileInDirection((Direction)-(int)direction, GetTile(tileIndex));
					tileIndex.second--;
					OpenTileInDirection(direction, GetTile(tileIndex));
				}
				else
					return false;
			}
			break;
		case Direction::Down:
			if (tileIndex.second == 0)
				return false;

			tileIndex.second--;
			if (!IsTileOpenInDirection((Direction)-(int)direction, GetTile(tileIndex)))
			{
				if (canDig)
				{
					OpenTileInDirection((Direction)-(int)direction, GetTile(tileIndex));
					tileIndex.second++;
					OpenTileInDirection(direction, GetTile(tileIndex));
				}
				else
					return false;
			}
			break;
		}
		return true;
	}
	return false;
}

std::pair<int,int> GridManager::TileIndexFromPosition(const Vector2& position)
{
	int xIndex = (int)(((position.x + (7 * g_TileDimensions.x)) / g_TileDimensions.x) + 0.5f);
	int yIndex = (int)(((position.y + (5 * g_TileDimensions.y)) / g_TileDimensions.y) + 0.5f);

	return std::make_pair(xIndex, yIndex);
}

const Vector2 GridManager::PositionFromTileIndex(int x, int y)
{
	float xPos = ((float)x * g_TileDimensions.x) - (7 * (int)g_TileDimensions.x);
	float yPos = ((float)y * g_TileDimensions.y) - (5 * g_TileDimensions.y);
	return Vector2(xPos, yPos);
}

const Vector2 GridManager::PositionFromTileIndex(int index)
{
	int xIndex = index % m_GridWidth;
	int yIndex = index / m_GridWidth;
	return PositionFromTileIndex(xIndex, yIndex);
}

GridTile& GridManager::GetTile(std::pair<int, int> index)
{
	return m_GridTiles[index.first + index.second * m_GridWidth];
}

bool GridManager::IsTileOpenAnyDirection(GridTile& tile)
{
	return tile.openDirections;
}

bool GridManager::IsTileOpenInDirection(Direction direction, GridTile& tile)
{
	switch (direction)
	{
	case Direction::Up:
		return (tile.openDirections & 0b0001);
	case Direction::Down:
		return (tile.openDirections & 0b0010);
	case Direction::Left:
		return (tile.openDirections & 0b0100);
	case Direction::Right:
		return (tile.openDirections & 0b1000);
	}
	return false;
}

void GridManager::OpenTileInDirection(Direction direction, GridTile& tile)
{
	switch (direction)
	{
	case Direction::Up:
		tile.openDirections = tile.openDirections | 0b0001;
		break;
	case Direction::Down:
		tile.openDirections = tile.openDirections | 0b0010;
		break;
	case Direction::Left:
		tile.openDirections = tile.openDirections | 0b0100;
		break;
	case Direction::Right:
		tile.openDirections = tile.openDirections | 0b1000;
		break;
	}
}

void GridManager::OpenTileInAllDirections(GridTile& tile)
{
	tile.openDirections = 0b1111;
}

void GridManager::OpenTileInAllDirections(int index)
{
	int xIndex = index % m_GridWidth;
	int yIndex = index / m_GridWidth;

	OpenTileInAllDirections(GetTile(std::make_pair(xIndex, yIndex)));
}

int GridManager::GetGridWidth() const
{
	return m_GridWidth;
}

int GridManager::GetGridHeight() const
{
	return m_GridHeight;
}


