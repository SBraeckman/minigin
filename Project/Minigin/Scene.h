#pragma once
#include "SceneManager.h"
#include "PhysicsHandler.h"

class Actor;

namespace dae
{
	class SceneObject;
	class Scene
	{
		friend Scene& SceneManager::CreateScene(const std::string& name);
	public:

		void Add(const std::shared_ptr<SceneObject>& object);
		virtual void Update(float elapsedTime);
		void Render() const;
		void SetControlledActor(std::weak_ptr<Actor> actor);
		virtual void OnActivate() {};

		Physics::PhysicsHandler& GetPhysics();
		std::shared_ptr<SceneObject> GetObjectWithTag(const std::string& tag);

		template <typename T>
		std::shared_ptr<T> GetObjectOfType()
		{
			for (std::shared_ptr<SceneObject> c : m_Objects)
			{
				std::shared_ptr<T> pTestPtr{ std::dynamic_pointer_cast<T>(c) };
				if (pTestPtr)
				{
					return pTestPtr;
				}
			}
			return std::shared_ptr<T>();
		}

		template <typename T>
		const std::shared_ptr<std::vector<std::shared_ptr<T>>> GetObjects()
		{
			std::shared_ptr<std::vector<std::shared_ptr<T>>> pointers = std::make_shared<std::vector<std::shared_ptr<T>>>();
			for (std::shared_ptr<SceneObject> c : m_Objects)
			{
				std::shared_ptr<T> pTestPtr{ std::dynamic_pointer_cast<T>(c) };
				if (pTestPtr)
				{
					pointers->push_back(pTestPtr);
				}
			}
			return pointers;
		}

		~Scene();
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;

	protected:
		explicit Scene(const std::string& name);
		virtual void HandleInputs();

	private: 

		std::string m_Name;
		std::vector < std::shared_ptr<SceneObject>> m_Objects{};
		std::weak_ptr<Actor> m_pControlledActor{};
		Physics::PhysicsHandler m_PhysicsHandler{};

		static unsigned int m_IdCounter;
	};

}
