#include "MiniginPCH.h"
#include "PhysicsHandler.h"
#include "Rigidbody.h"
#include "Collider.h"
#include "GameObject.h"
#include <algorithm>

void Physics::PhysicsHandler::HandlePhysics(float elapsedTime)
{
	Vector3 velocity{};
	Vector3 gravity{ m_Gravity };

	//This lambda moves the transforms of the rigidbodies according to their velocity
	auto moveRBsLambda = [elapsedTime, velocity, gravity](std::weak_ptr<Rigidbody> r) mutable
	{
		velocity = r.lock()->GetVelocity();
		velocity += gravity * elapsedTime;
		dae::Transform& rbTransform = r.lock()->GetPhysicsTransform();
		rbTransform.SetPosition(rbTransform.GetPosition() + velocity * elapsedTime);
		r.lock()->SetVelocity(velocity);
	};

	std::for_each(m_pDynamicRigidBodies.begin(), m_pDynamicRigidBodies.end(), moveRBsLambda);

	for (int dynIndex{}; dynIndex < (int)m_pDynamicRigidBodies.size(); dynIndex++)
	{
		if (m_pDynamicRigidBodies[dynIndex].lock()->GetGameObject().lock()->IsEnabled())
		{
			for (int statIndex{}; statIndex < (int)m_pStaticRigidBodies.size(); statIndex++)
			{
				if (m_pStaticRigidBodies[statIndex].lock()->GetGameObject().lock()->IsEnabled())
				{
					const std::vector<std::weak_ptr<Collider>>& dynColliders = m_pDynamicRigidBodies[dynIndex].lock()->GetColliders();
					const std::vector<std::weak_ptr<Collider>>& statColliders = m_pStaticRigidBodies[statIndex].lock()->GetColliders();

					for (std::weak_ptr<Collider> c : dynColliders)
					{
						for (std::weak_ptr<Collider> cs : statColliders)
						{
							CollideAndResolve(c, cs, m_pDynamicRigidBodies[dynIndex], m_pStaticRigidBodies[statIndex]);
						}
					}
				}
			}

			for (int dynIndex2{ dynIndex + 1 }; dynIndex2 < (int)m_pDynamicRigidBodies.size(); dynIndex2++)
			{
				if (m_pDynamicRigidBodies[dynIndex2].lock()->GetGameObject().lock()->IsEnabled())
				{
					const std::vector<std::weak_ptr<Collider>>& firstColliders = m_pDynamicRigidBodies[dynIndex].lock()->GetColliders();
					const std::vector<std::weak_ptr<Collider>>& secondColliders = m_pDynamicRigidBodies[dynIndex2].lock()->GetColliders();

					for (std::weak_ptr<Collider> c : firstColliders)
					{
						for (std::weak_ptr<Collider> cs : secondColliders)
						{
							CollideAndResolve(c, cs, m_pDynamicRigidBodies[dynIndex], m_pDynamicRigidBodies[dynIndex2]);
						}
					}
				}
			}
		}
	}


	for (std::weak_ptr<Rigidbody> rb : m_pRigidBodies)
	{
		rb.lock()->UpdateGameObjectPosition();
	}

}

void Physics::PhysicsHandler::AddRigidbody(std::weak_ptr<Physics::Rigidbody> rb)
{
	m_pRigidBodies.push_back(rb);
	if (rb.lock()->GetType() == Physics::RigidbodyType::Static)
	{
		m_pStaticRigidBodies.push_back(rb);
	}
	else
	{
		m_pDynamicRigidBodies.push_back(rb);
	}
}

void Physics::PhysicsHandler::CollideAndResolve(std::weak_ptr<Physics::Collider> dyn, std::weak_ptr<Physics::Collider> stat, std::weak_ptr<Physics::Rigidbody> dynRigidbody, std::weak_ptr<Physics::Rigidbody> statRigidbody)
{
	Physics::ColliderInfo dynamicC = dyn.lock()->GetInfo();
	Physics::ColliderInfo staticC = stat.lock()->GetInfo();
	dynamicC.center += dynRigidbody.lock()->GetPhysicsTransform().GetPosition();
	staticC.center += statRigidbody.lock()->GetPhysicsTransform().GetPosition();
	Vector3 velocity = dynRigidbody.lock()->GetVelocity();
	bool verticalTest = abs(dynamicC.center.y - staticC.center.y) < dynamicC.height / 2 + staticC.height / 2;
	bool horizontalTest = abs(dynamicC.center.x - staticC.center.x) < dynamicC.width / 2 + staticC.width / 2;
	if (verticalTest && horizontalTest)
	{
		//This function call uses the "passkey" pattern, to make sure that only the physicshandler can tell colliders they collided with something
		dyn.lock()->CollisionHappened(stat, 0);
		stat.lock()->CollisionHappened(dyn, 0);
	}
}
