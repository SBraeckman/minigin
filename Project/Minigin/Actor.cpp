#include "MiniginPCH.h"
#include "Actor.h"
#include "CharacterController.h"
#include "Rigidbody.h"
#include "BoxCollider.h"

void Actor::Move(Direction direction)
{
	m_pCharControl->TryMove(direction, m_Speed);
}

void Actor::Fire()
{
}

Actor::Actor()
	: GameObject()
{
	std::shared_ptr<Physics::Rigidbody> rb = std::make_shared<Physics::Rigidbody>();
	AddComponent(rb);
	rb->SetType(Physics::RigidbodyType::Dynamic);
	std::shared_ptr<CharacterController> CharControl = std::make_shared<CharacterController>();
	AddComponent(CharControl);
	CharControl->SetRigidbody(rb);
	m_pCharControl = CharControl;
	auto boxCollider = std::make_shared<BoxCollider>();
	boxCollider->SetWidth(16);
	boxCollider->SetHeight(16);
	boxCollider->SetPosition(Vector3(0, 0, 0));
	boxCollider->SetTrigger(true);
	AddComponent(boxCollider);
	rb->AddCollider(boxCollider);
}
