#pragma once
#include <memory>

namespace dae
{
	class Transform;
	class GameObject;
}

class Component 
{
public:
	virtual void Update() = 0;
	virtual void Render() const = 0;

	virtual void OnAdd() {}

	std::weak_ptr<dae::Transform> GetTransform() const
	{
		return m_pTransform;
	}

	std::weak_ptr<dae::GameObject> GetGameObject() const
	{
		return m_pGameObject;
	}

	void SetTransform(std::weak_ptr<dae::Transform> transform)
	{
		m_pTransform = transform;
	}

	void SetGameObject(std::weak_ptr < dae::GameObject> gameObject)
	{
		m_pGameObject = gameObject;
	}

	Component() = default;
	virtual ~Component() = default;
	Component(const Component& other) = delete;
	Component(Component&& other) = delete;
	Component& operator=(const Component& other) = delete;
	Component& operator=(Component&& other) = delete;

private:

	std::weak_ptr<dae::Transform> m_pTransform{};
	std::weak_ptr<dae::GameObject> m_pGameObject{};

};

