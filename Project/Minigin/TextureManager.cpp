#include "MiniginPCH.h"
#include "TextureManager.h"
#include "ResourceManager.h"
#include "Texture2D.h"

void TextureManager::AddTexture(const std::string& path, const std::string& name)
{
	std::shared_ptr<dae::Texture2D> texture = dae::ResourceManager::GetInstance().LoadTexture(path);
	m_Textures[name] = texture;
}

void TextureManager::AddTexture(std::shared_ptr<dae::Texture2D> texture, const std::string& name)
{
	m_Textures[name] = texture;
}

std::weak_ptr<dae::Texture2D> TextureManager::GetTexture(const std::string& name)
{
	return std::weak_ptr<dae::Texture2D>(m_Textures[name]);
}
