#pragma once
#include "GameObject.h"

class CharacterController;

class Actor :
	public dae::GameObject
{
public:

	virtual void Move(Direction direction);
	virtual void Fire();

	void SetSpeed(int speed)
	{
		m_Speed = speed;
	}

	std::weak_ptr<CharacterController> GetCharControl()
	{
		return m_pCharControl;
	}

	explicit Actor();
	virtual ~Actor() = default;

	void SetStartPosition(const Vector3& position)
	{
		m_startPosition = position;
	}

	void SetStartPosition(const Vector2& position)
	{
		m_startPosition.x = position.x;
		m_startPosition.y = position.y;
		m_startPosition.z = 0;
	}

protected:

	Vector3 m_startPosition{};


private:

	int m_Speed{ 0 };
	std::shared_ptr<CharacterController> m_pCharControl{};

};

