#pragma once
#include "Singleton.h"
#include <unordered_map>

namespace dae
{
	class Font;
}

class FontManager : public dae::Singleton<FontManager>
{
public:
	void AddFont(const std::string& path, const std::string& name, int fontSize);
	std::weak_ptr<dae::Font> GetFont(const std::string& name);

private:
	friend class dae::Singleton<FontManager>;
	FontManager() = default;
	std::unordered_map<std::string, std::shared_ptr<dae::Font>> m_Fonts{};
};

