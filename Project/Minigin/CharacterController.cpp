#include "MiniginPCH.h"
#include "CharacterController.h"
#include "Rigidbody.h"
#include "GridManager.h"
#include "Transform.h"

void CharacterController::Update()
{
	if (m_MoveThisFrame)
	{
		if (abs((int)m_CurrentDirection) == 2)
		{
			m_pGORigidbody->SetVelocity(Vector3((float)m_CurrentSpeed * (int)m_CurrentDirection / 2, 0, 0));
		}
		else
		{
			m_pGORigidbody->SetVelocity(Vector3(0, (float)m_CurrentSpeed * (int)m_CurrentDirection, 0));
		}
		m_MoveThisFrame = false;
	}
	else
	{
		m_pGORigidbody->SetVelocity(Vector3(0, 0, 0));
	}
}

void CharacterController::TryMove(Direction direction, int speed)
{
	Vector3 currentPosition{ GetTransform().lock()->GetPosition() };

	if (IsOnGrid(currentPosition))
	{
		if (GridManager::GetInstance().IsValidMovement(Vector2(GetTransform().lock()->GetPosition().x, GetTransform().lock()->GetPosition().y), direction, m_CanDig))
		{
			Move(direction, speed);
		}
		else
			m_pGORigidbody->SetVelocity(Vector3(0, 0, 0));
	}
	else
	{
		if (direction == m_CurrentDirection || (int)direction == -(int)m_CurrentDirection)
		{
			Move(direction, speed);
		}
		else
		{
			Move(m_CurrentDirection, m_CurrentSpeed);
		}
	}
}

void CharacterController::SetRigidbody(std::shared_ptr<Physics::Rigidbody> rb)
{
	m_pGORigidbody = rb;
}

Direction CharacterController::GetDirection() const
{
	return m_CurrentDirection;
}

void CharacterController::Move(Direction direction, int speed)
{
	m_CurrentDirection = direction;
	m_CurrentSpeed = speed;
	m_MoveThisFrame = true;
}
