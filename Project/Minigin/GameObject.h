#pragma once
#include "Transform.h"
#include "SceneObject.h"
#include "Observers.h"
#include <vector>

class Component;
namespace dae
{
	class Texture2D;
	class GameObject : public SceneObject , public CollisionObserver
	{
	public:
		virtual void Update() override;
		virtual void Render() const override;
		virtual void LateUpdate() override {}

		void SetPosition(float x, float y);
		void SetPosition(const Vector2& pos);
		void AddComponent(std::shared_ptr<Component> component);
		void SetForcedMove(bool value);
		virtual void SetScenePtr(std::weak_ptr<SceneObject> ptr) override;

		std::weak_ptr<Transform> GetTransform();
		bool HasBeenMoved();
		
		virtual void OnTriggerEnter(std::weak_ptr<Physics::Collider> other);

		template <typename T>
		std::shared_ptr<T> GetComponent()
		{
			for (std::shared_ptr<Component> c : m_pComponents)
			{
				std::shared_ptr<T> pTestPtr{ std::dynamic_pointer_cast<T>(c) };
				if (pTestPtr)
				{
					return pTestPtr;
				}
			}
			return std::shared_ptr<T>();
		}

		template <typename T>
		const std::shared_ptr<std::vector<std::shared_ptr<T>>> GetComponents()
		{
			std::shared_ptr<std::vector<std::shared_ptr<T>>> pointers = std::make_shared<std::vector<std::shared_ptr<T>>>();
			for (std::shared_ptr<Component> c : m_pComponents)
			{
				std::shared_ptr<T> pTestPtr{ std::dynamic_pointer_cast<T>(c) };
				if (pTestPtr != nullptr)
				{
					pointers->push_back(pTestPtr);
				}
			}
			return pointers;
		}

		GameObject() = default;
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	private:

		bool m_ForcedMove{false};
		std::weak_ptr<GameObject> m_pThis{};
		std::shared_ptr<Transform> m_pTransform{std::make_shared<Transform>()};
		std::vector<std::shared_ptr<Component>> m_pComponents{};
	};
}
