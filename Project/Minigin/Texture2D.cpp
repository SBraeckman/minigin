#include "MiniginPCH.h"
#include "Texture2D.h"
#include <SDL.h>
#include "SDL_render.h"

dae::Texture2D::~Texture2D()
{
	SDL_DestroyTexture(m_Texture);
}

int dae::Texture2D::GetWidth()
{
	int w{};
	SDL_QueryTexture(m_Texture, NULL, NULL, &w, NULL);
	return w;
}

int dae::Texture2D::GetHeight()
{
	int h{};
	SDL_QueryTexture(m_Texture, NULL, NULL, NULL, &h);
	return h;
}

SDL_Texture* dae::Texture2D::GetSDLTexture() const
{
	return m_Texture;
}

dae::Texture2D::Texture2D(SDL_Texture* texture)
{
	m_Texture = texture;
}
