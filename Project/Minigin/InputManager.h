#pragma once
#include <XInput.h>
#include "Singleton.h"
#include "Commands.h"

namespace dae
{
	enum class ControllerButton
	{
		ButtonA,
		ButtonB,
		ButtonX,
		ButtonY,
		DpadDown,
		DpadUp,
		DpadLeft,
		DpadRight
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		std::shared_ptr<Command> HandleInput();
		std::shared_ptr<Command> HandleContinuousInput();
		bool IsPressed(ControllerButton button) const;
		bool HasQuitHappened() const;
		~InputManager() = default;
	private:
		XINPUT_STATE m_CurrentState{};
		bool m_QuitHappened{ false };


		std::shared_ptr<Command> m_pSpaceCommand{std::make_shared<FireCommand>()};
		std::shared_ptr<Command> m_pArrowUpCommand{std::make_shared<UpCommand>()};
		std::shared_ptr<Command> m_pArrowDownCommand{std::make_shared<DownCommand>()};
		std::shared_ptr<Command> m_pArrowRightCommand{std::make_shared<RightCommand>()};
		std::shared_ptr<Command> m_pArrowLeftCommand{std::make_shared<LeftCommand>()};
	};

}
