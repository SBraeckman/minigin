#pragma once
#include "Singleton.h"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Rect;

namespace dae
{
	class Texture2D;
	/**
	 * Simple RAII wrapper for the SDL renderer
	 */
	class Renderer final : public Singleton<Renderer>
	{
	public:
		void Init(SDL_Window* window);
		void Render() const;
		void Destroy();

		void RenderTexture(const Texture2D& texture, float x, float y) const;
		void RenderTexture(const Texture2D& texture, float x, float y, const SDL_Rect& rect) const;

		SDL_Renderer* GetSDLRenderer() const { return m_Renderer; }
		
	private:
		SDL_Renderer* m_Renderer{};
		const float m_PixelsPerUnit{ 3.f };
		const Vector2 m_OriginOffset{480, 300};
	};
}

