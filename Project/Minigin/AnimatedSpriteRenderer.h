#pragma once
#include "SpriteRenderer.h"

struct SDL_Rect;

class AnimatedSpriteRenderer : public SpriteRenderer
{
public:

	virtual void Update() override;
	virtual void Render() const override;

	void Play();
	void Stop();
	void Pause();

	void SetColNr(int nr);
	void SetRowNr(int nr);

	virtual void SetTexture(const std::string& name);
	virtual void SetTexture(std::weak_ptr<dae::Texture2D> tex);

	virtual void SetVariables(int frameWidth, int frameHeight, float frameTime, bool isPlaying);

	AnimatedSpriteRenderer() = default;
	virtual ~AnimatedSpriteRenderer() = default;
	AnimatedSpriteRenderer(const AnimatedSpriteRenderer& other) = delete;
	AnimatedSpriteRenderer(AnimatedSpriteRenderer&& other) = delete;
	AnimatedSpriteRenderer& operator=(const AnimatedSpriteRenderer& other) = delete;
	AnimatedSpriteRenderer& operator=(AnimatedSpriteRenderer&& other) = delete;

private:

	std::pair<int, int> m_FrameSize{};

	float m_FrameTime{};
	float m_FrameTimer{};

	int m_texWidth{};
	int m_texHeight{};
	int m_colNr{};
	int m_rowNr{};
	
	bool m_IsPlaying{false};

};


