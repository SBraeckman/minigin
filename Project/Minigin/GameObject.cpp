#include "MiniginPCH.h"
#include "GameObject.h"
#include "Component.h"
#include "Subjects.h"

dae::GameObject::~GameObject() = default;

void dae::GameObject::Update()
{
	for (std::shared_ptr<Component> c : m_pComponents)
	{
		c->Update();
	}
}

void dae::GameObject::Render() const
{
	for (std::shared_ptr<Component> c : m_pComponents)
	{
		c->Render();
	}
}

void dae::GameObject::SetPosition(float x, float y)
{
	m_pTransform->SetPosition(x, y, 0.0f);
	m_ForcedMove = true;
}

void dae::GameObject::SetPosition(const Vector2& pos)
{
	SetPosition(pos.x, pos.y);
}

void dae::GameObject::AddComponent(std::shared_ptr<Component> component)
{
	component->SetTransform(m_pTransform);
	component->SetGameObject(std::static_pointer_cast<GameObject>(GetScenePtr().lock()));
	m_pComponents.push_back(component);
	component->OnAdd();
}

void dae::GameObject::SetForcedMove(bool value)
{
	m_ForcedMove = value;
}

void dae::GameObject::SetScenePtr(std::weak_ptr<SceneObject> ptr)
{
	SceneObject::SetScenePtr(ptr);
	for (std::shared_ptr<Component> c : m_pComponents)
	{
		c->SetGameObject(std::static_pointer_cast<GameObject>(ptr.lock()));
	}

	//GROSS CODE!! CLEANUP!!!
	auto cs = GetComponent<CollisionSubject>();
	if (cs)
	{
		cs->AddObserver(std::static_pointer_cast<CollisionObserver>(std::static_pointer_cast<GameObject>(ptr.lock())));
	}
}

std::weak_ptr<dae::Transform> dae::GameObject::GetTransform()
{
	return std::weak_ptr<dae::Transform>(m_pTransform);
}

bool dae::GameObject::HasBeenMoved()
{
	return m_ForcedMove;
}

void dae::GameObject::OnTriggerEnter(std::weak_ptr<Physics::Collider> other)
{
}
