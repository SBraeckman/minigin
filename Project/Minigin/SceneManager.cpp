#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"

void dae::SceneManager::Update(float elapsedTime)
{
	m_pScenes[m_ActiveScene]->Update(elapsedTime);
}

void dae::SceneManager::Render()
{
	m_pScenes[m_ActiveScene]->Render();
}

dae::Scene& dae::SceneManager::CreateScene(const std::string& name)
{
	const auto scene = std::shared_ptr<Scene>(new Scene(name));
	m_pScenes[name] = scene;
	return *scene;
}

void dae::SceneManager::AddScene(const std::string& name, std::shared_ptr<Scene> scene)
{
	m_pScenes[name] = scene;
}

void dae::SceneManager::SetActiveScene(const std::string& name)
{
	if (m_pScenes[name])
	{
		m_ActiveScene = name;
		m_pScenes[name]->OnActivate();
	}
}

std::weak_ptr<dae::Scene> dae::SceneManager::GetActiveScene() const
{
	return m_pScenes.at(m_ActiveScene);
}

std::weak_ptr<dae::Scene> dae::SceneManager::GetScene(const std::string& name)
{
	return m_pScenes[name];
}
