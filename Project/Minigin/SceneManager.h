#pragma once
#include "Singleton.h"
#include <unordered_map>

namespace dae
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		Scene& CreateScene(const std::string& name);
		void AddScene(const std::string& name, std::shared_ptr<Scene> scene);
		
		void SetActiveScene(const std::string& name);
		std::weak_ptr<Scene> GetActiveScene() const;
		std::weak_ptr<Scene> GetScene(const std::string& name);

		void Update(float elapsedTime);
		void Render();

	private:
		friend class Singleton<SceneManager>;
		SceneManager() = default;
		std::unordered_map<std::string, std::shared_ptr<Scene>> m_pScenes{};
		std::string m_ActiveScene{};
	};
}
