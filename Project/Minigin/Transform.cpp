#include "MiniginPCH.h"
#include "Transform.h"

void dae::Transform::SetPosition(const float x, const float y, const float z)
{
	m_Position.x = x;
	m_Position.y = y;
	m_Position.z = z;
}

void dae::Transform::SetPosition(const Vector3& position)
{
	m_Position = position;
}

void dae::Transform::SetPosition(const Vector2& position)
{
	m_Position.x = position.x;
	m_Position.y = position.y;
	m_Position.z = 0;
}
