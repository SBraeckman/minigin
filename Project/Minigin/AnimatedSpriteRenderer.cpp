#include "MiniginPCH.h"
#include "AnimatedSpriteRenderer.h"
#include "SDL.h"
#include "Minigin.h"
#include "Texture2D.h"
#include "Renderer.h"
#include "Transform.h"

void AnimatedSpriteRenderer::Update()
{
	if (!m_IsPlaying)
		return;

	m_FrameTimer -= dae::Minigin::g_DeltaTime;
	if (m_FrameTimer <= 0)
	{
		SetColNr(m_colNr + 1);
		m_FrameTimer = m_FrameTime;
	}
}

void AnimatedSpriteRenderer::Render() const
{
	SDL_Rect rect;

	rect.x = m_colNr * m_FrameSize.first;
	rect.y = m_rowNr * m_FrameSize.second;
	rect.w = m_FrameSize.first;
	rect.h = m_FrameSize.second;


	Vector3 pos = GetTransform().lock()->GetPosition();
	dae::Renderer::GetInstance().RenderTexture(*m_pTexture.lock(), pos.x, pos.y, rect);
}

void AnimatedSpriteRenderer::Play()
{
	if (!m_IsPlaying)
		m_IsPlaying = true;
}

void AnimatedSpriteRenderer::Stop()
{
	m_IsPlaying = false;
	m_colNr = 0;
}

void AnimatedSpriteRenderer::Pause()
{
	m_IsPlaying = false;
}

void AnimatedSpriteRenderer::SetColNr(int nr)
{
	m_colNr = nr % (m_texWidth / m_FrameSize.first);
}

void AnimatedSpriteRenderer::SetRowNr(int nr)
{
	m_rowNr = nr % (m_texHeight / m_FrameSize.second);
}

void AnimatedSpriteRenderer::SetTexture(const std::string& name)
{
	SpriteRenderer::SetTexture(name);
	auto tex = GetTexture();
	m_texHeight = tex.lock()->GetHeight();
	m_texWidth = tex.lock()->GetWidth();
}

void AnimatedSpriteRenderer::SetTexture(std::weak_ptr<dae::Texture2D> tex)
{
	SpriteRenderer::SetTexture(tex);
	m_texHeight = tex.lock()->GetHeight();
	m_texWidth = tex.lock()->GetWidth();
}

void AnimatedSpriteRenderer::SetVariables(int frameWidth, int frameHeight, float frameTime, bool isPlaying)
{
	m_FrameSize.first = frameWidth;
	m_FrameSize.second = frameHeight;
	m_FrameTime = frameTime;
	m_FrameTimer = frameTime;
	m_IsPlaying = isPlaying;
}

