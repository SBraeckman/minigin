#pragma once
#include "Component.h"
#include "Transform.h"

namespace Physics
{
	class Collider;

	enum class RigidbodyType
	{
		Dynamic,
		Static
	};


	class Rigidbody final : public Component
	{
	public:

		virtual void Update() override;
		virtual void Render() const override;
		virtual void OnAdd() override;

		void SetAffectedByGravity(bool value);
		void SetType(RigidbodyType type);
		void SetNewTransform(std::weak_ptr<dae::Transform> newTransform);
		void SetVelocity(const Vector3& velocity);
		void UpdateGameObjectPosition();
		void AddCollider(std::weak_ptr<Collider> collider);

		dae::Transform& GetPhysicsTransform();
		const Vector3& GetVelocity() const;
		bool IsAffectedByGravity() const;
		RigidbodyType GetType() const;
		const std::vector<std::weak_ptr<Collider>>& GetColliders() const;

		Rigidbody() = default;
		virtual ~Rigidbody() = default;
		Rigidbody(const Rigidbody& other) = delete;
		Rigidbody(Rigidbody&& other) = delete;
		Rigidbody& operator=(const Rigidbody& other) = delete;
		Rigidbody& operator=(Rigidbody&& other) = delete;

	private:
		std::vector<std::weak_ptr<Collider>> m_pColliders{};
		RigidbodyType m_Type{RigidbodyType::Dynamic};
		Vector3 m_Velocity{Vector3(0,0,0)};
		dae::Transform m_PhysicsTransform{};
		bool m_Gravity{true};
	};
}

