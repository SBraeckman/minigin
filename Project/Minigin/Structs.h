#pragma once

enum class Direction
{
	Up = 1,
	Down = -1,
	Right = 2,
	Left = -2
};

struct Vector3
{
	float x;
	float y;
	float z;

	Vector3()
		: x{ 0 }
		, y{ 0 }
		, z{ 0 }
	{}
	Vector3(float x, float y, float z)
		: x{ x }
		, y{ y }
		, z{ z }
	{}

	Vector3& operator+=(const Vector3& right)
	{
		x += right.x;
		y += right.y;
		z += right.z;
		return *this;
	}

	Vector3& operator*=(float right)
	{
		x *= right;
		y *= right;
		z *= right;
		return *this;
	}

	Vector3& operator*=(int right)
	{
		return *this *= float(right);
	}
};

struct Vector2
{
	float x;
	float y;

	Vector2()
		: x{0}
		, y{0}
	{}

	Vector2(float x, float y)
		: x{ x }
		, y{ y }
	{}

	Vector2(const Vector3& v)
		: x{ v.x }
		, y{ v.y }
	{}

};

inline Vector3 operator+(const Vector3& left, const Vector3& right)
{
	Vector3 newVector{};
	newVector.x = left.x + right.x;
	newVector.y = left.y + right.y;
	newVector.z = left.z + right.z;
	return newVector;
}

inline Vector3 operator-(const Vector3& left, const Vector3& right)
{
	Vector3 newVector{};
	newVector.x = left.x - right.x;
	newVector.y = left.y - right.y;
	newVector.z = left.z - right.z;
	return newVector;
}

inline Vector3 operator*(const Vector3& left, float right)
{
	Vector3 newVector{};
	newVector.x = left.x * right;
	newVector.y = left.y * right;
	newVector.z = left.z * right;
	return newVector;
}

inline Vector3 operator*(const Vector3& left, int right)
{
	return left * float(right);
}
