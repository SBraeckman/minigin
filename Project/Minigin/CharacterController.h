#pragma once
#include "Component.h"

namespace Physics
{
	class Rigidbody;
}

class CharacterController final :
	public Component
{
public:

	virtual void Update() override;
	virtual void Render() const override {};

	void SetCanDig(bool value)
	{
		m_CanDig = value;
	}

	CharacterController() = default;
	virtual ~CharacterController() = default;
	CharacterController(const CharacterController& other) = delete;
	CharacterController(CharacterController&& other) = delete;
	CharacterController& operator=(const CharacterController& other) = delete;
	CharacterController& operator=(CharacterController&& other) = delete;

	void TryMove(Direction direction, int speed);
	void SetRigidbody(std::shared_ptr<Physics::Rigidbody> rb);

	Direction GetDirection() const;

private:

	std::shared_ptr<Physics::Rigidbody> m_pGORigidbody{};
	Direction m_CurrentDirection{ Direction::Up };
	int m_CurrentSpeed{ 0 };
	bool m_MoveThisFrame{ false };
	bool m_CanDig{ false };

	void Move(Direction direction, int speed);

};

