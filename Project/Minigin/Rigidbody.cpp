#include "MiniginPCH.h"
#include "Rigidbody.h"
#include "Collider.h"
#include "GameObject.h"

void Physics::Rigidbody::SetNewTransform(std::weak_ptr<dae::Transform> newTransform)
{
	m_PhysicsTransform.SetPosition(newTransform.lock()->GetPosition());
}

void Physics::Rigidbody::SetVelocity(const Vector3& velocity)
{
	m_Velocity = velocity;
}

void Physics::Rigidbody::UpdateGameObjectPosition()
{
	if (GetGameObject().lock()->HasBeenMoved())
	{
		m_PhysicsTransform.SetPosition(GetGameObject().lock()->GetTransform().lock()->GetPosition());
	}
	else
	{
		GetGameObject().lock()->SetPosition(m_PhysicsTransform.GetPosition().x, m_PhysicsTransform.GetPosition().y);
	}
	GetGameObject().lock()->SetForcedMove(false);
}

void Physics::Rigidbody::AddCollider(std::weak_ptr<Collider> collider)
{
	m_pColliders.push_back(collider);
}

const Vector3& Physics::Rigidbody::GetVelocity() const
{
	return m_Velocity;
}

bool Physics::Rigidbody::IsAffectedByGravity() const
{
	return m_Gravity;
}

Physics::RigidbodyType Physics::Rigidbody::GetType() const
{
	return m_Type;
}

dae::Transform& Physics::Rigidbody::GetPhysicsTransform()
{
	return m_PhysicsTransform;
}

const std::vector<std::weak_ptr<Physics::Collider>>& Physics::Rigidbody::GetColliders() const
{
	return m_pColliders;
}

void Physics::Rigidbody::Update()
{
	SetNewTransform(GetGameObject().lock()->GetTransform());
}

void Physics::Rigidbody::Render() const
{
}

void Physics::Rigidbody::OnAdd()
{
	if (GetGameObject().lock())
	{
		for (std::shared_ptr<Collider> c : *GetGameObject().lock()->GetComponents<Collider>())
		{
			AddCollider(c);
		}
	}
}

void Physics::Rigidbody::SetAffectedByGravity(bool value)
{
	m_Gravity = value;
}

void Physics::Rigidbody::SetType(RigidbodyType type)
{
	m_Type = type;
}
