#pragma once
namespace dae
{
	class SceneObject
	{
	public:
		virtual void Update() = 0;
		virtual void Render() const = 0;
		virtual void LateUpdate() = 0;

		virtual void SetScenePtr(std::weak_ptr<SceneObject> ptr)
		{
			m_pThis = ptr;
		}

		std::weak_ptr<SceneObject> GetScenePtr() const
		{
			return m_pThis;
		}

		void SetEnabled(bool value)
		{
			m_Enabled = value;
		}

		bool IsEnabled()
		{
			return m_Enabled;
		}

		void SetTag(const std::string& tag)
		{
			m_Tag = tag;
		}

		const std::string& GetTag() const
		{
			return m_Tag;
		}

		SceneObject() = default;
		virtual ~SceneObject() = default;
		SceneObject(const SceneObject& other) = delete;
		SceneObject(SceneObject&& other) = delete;
		SceneObject& operator=(const SceneObject& other) = delete;
		SceneObject& operator=(SceneObject&& other) = delete;
		
	private:
		std::weak_ptr<SceneObject> m_pThis{};
		bool m_Enabled{true};
		std::string m_Tag{};
	};
}
