#pragma once
#include "Component.h"

namespace dae
{
	class Font;
	class Texture2D;
}

class Text : public Component
{
public:

	virtual void Update() override;
	virtual void Render() const override;

	void SetText(const std::string& text);

	void SetFont(const std::string& name);
	void SetFont(std::weak_ptr<dae::Font> font);

private:

	std::string m_Text{};
	std::weak_ptr<dae::Font> m_pFont{};
	std::shared_ptr<dae::Texture2D> m_pTexture{};
	bool m_NeedsUpdate{false};

};

