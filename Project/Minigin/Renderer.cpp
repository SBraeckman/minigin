#include "MiniginPCH.h"
#include "Renderer.h"
#include <SDL.h>
#include "SceneManager.h"
#include "Texture2D.h"

void dae::Renderer::Init(SDL_Window * window)
{
	m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_Renderer == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateRenderer Error: ") + SDL_GetError());
	}
}

void dae::Renderer::Render() const
{
	SDL_RenderClear(m_Renderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(m_Renderer);
}

void dae::Renderer::Destroy()
{
	if (m_Renderer != nullptr)
	{
		SDL_DestroyRenderer(m_Renderer);
		m_Renderer = nullptr;
	}
}

void dae::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x * m_PixelsPerUnit);
	dst.y = static_cast<int>(y * m_PixelsPerUnit);
	dst.y *= -1;
	dst.x += (int)m_OriginOffset.x;
	dst.y += (int)m_OriginOffset.y;
	SDL_QueryTexture(texture.GetSDLTexture(), nullptr, nullptr, &dst.w, &dst.h);
	dst.w = (int)(dst.w * m_PixelsPerUnit);
	dst.h = (int)(dst.h * m_PixelsPerUnit);
	dst.x -= dst.w / 2;
	dst.y -= dst.h / 2;

	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void dae::Renderer::RenderTexture(const Texture2D& texture, float x, float y, const SDL_Rect& rect) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x * m_PixelsPerUnit);
	dst.y = static_cast<int>(y * m_PixelsPerUnit);
	dst.y *= -1;
	dst.x += (int)m_OriginOffset.x;
	dst.y += (int)m_OriginOffset.y;
	SDL_QueryTexture(texture.GetSDLTexture(), nullptr, nullptr, &dst.w, &dst.h);
	dst.w = (int)(rect.w * m_PixelsPerUnit);
	dst.h = (int)(rect.h * m_PixelsPerUnit);
	dst.x -= dst.w / 2;
	dst.y -= dst.h / 2;

	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), &rect, &dst);
}
