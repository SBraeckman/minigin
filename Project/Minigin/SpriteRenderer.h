#pragma once
#include "Component.h"

namespace dae
{
	class Texture2D;
}

class SpriteRenderer : public Component
{
public:

	virtual void Update() override {};
	virtual void Render() const override;
	
	virtual void SetTexture(const std::string& name);
	virtual void SetTexture(std::weak_ptr<dae::Texture2D> tex);
	std::weak_ptr<dae::Texture2D> GetTexture();

	SpriteRenderer() = default;
	virtual ~SpriteRenderer() = default;
	SpriteRenderer(const SpriteRenderer& other) = delete;
	SpriteRenderer(SpriteRenderer&& other) = delete;
	SpriteRenderer& operator=(const SpriteRenderer& other) = delete;
	SpriteRenderer& operator=(SpriteRenderer&& other) = delete;

protected:

	std::weak_ptr<dae::Texture2D> m_pTexture{};
};

