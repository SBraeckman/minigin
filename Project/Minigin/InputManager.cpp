#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>

std::shared_ptr<Command> dae::InputManager::HandleInput()
{

	ZeroMemory(&m_CurrentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &m_CurrentState);

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT)
		{
			m_QuitHappened = true;
			return nullptr;
		}
		if (e.type == SDL_KEYDOWN) {
			if (e.key.keysym.scancode == SDL_SCANCODE_SPACE)
			{
				return m_pSpaceCommand;
			}
		}
	}

	return nullptr;
}

std::shared_ptr<Command> dae::InputManager::HandleContinuousInput()
{
	ZeroMemory(&m_CurrentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &m_CurrentState);

	const Uint8* keyState = SDL_GetKeyboardState(NULL);

	if (keyState[SDL_SCANCODE_LEFT])
	{
		return m_pArrowLeftCommand;
	}
	if (keyState[SDL_SCANCODE_RIGHT])
	{
		return m_pArrowRightCommand;
	}
	if (keyState[SDL_SCANCODE_UP])
	{
		return m_pArrowUpCommand;
	}
	if (keyState[SDL_SCANCODE_DOWN])
	{
		return m_pArrowDownCommand;
	}
	if (IsPressed(ControllerButton::DpadLeft))
	{
		return m_pArrowLeftCommand;
	}
	if (IsPressed(ControllerButton::DpadRight))
	{
		return m_pArrowRightCommand;
	}
	if (IsPressed(ControllerButton::DpadUp))
	{
		return m_pArrowUpCommand;
	}
	if (IsPressed(ControllerButton::DpadDown))
	{
		return m_pArrowDownCommand;
	}
	if (IsPressed(ControllerButton::ButtonA))
	{
		return m_pSpaceCommand;
	}

	return nullptr;
}

bool dae::InputManager::IsPressed(ControllerButton button) const
{
	switch (button)
	{
	case ControllerButton::ButtonA:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_A;
	case ControllerButton::ButtonB:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	case ControllerButton::ButtonX:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	case ControllerButton::ButtonY:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	case ControllerButton::DpadDown:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
	case ControllerButton::DpadUp:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
	case ControllerButton::DpadLeft:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
	case ControllerButton::DpadRight:
		return m_CurrentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;
	default: return false;
	}
}

bool dae::InputManager::HasQuitHappened() const
{
	return m_QuitHappened;
}


