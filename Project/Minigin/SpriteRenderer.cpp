#include "MiniginPCH.h"
#include "SpriteRenderer.h"
#include "Renderer.h"
#include "Transform.h"
#include "TextureManager.h"

void SpriteRenderer::Render() const
{
	Vector3 pos = GetTransform().lock()->GetPosition();
	dae::Renderer::GetInstance().RenderTexture(*m_pTexture.lock(), pos.x, pos.y);
}

void SpriteRenderer::SetTexture(const std::string& name)
{
	m_pTexture = TextureManager::GetInstance().GetTexture(name);
}

void SpriteRenderer::SetTexture(std::weak_ptr<dae::Texture2D> tex)
{
	m_pTexture = tex;
}

std::weak_ptr<dae::Texture2D> SpriteRenderer::GetTexture()
{
	return m_pTexture;
}
