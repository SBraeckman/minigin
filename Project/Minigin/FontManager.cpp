#include "MiniginPCH.h"
#include "FontManager.h"
#include "ResourceManager.h"

void FontManager::AddFont(const std::string& path, const std::string& name, int fontSize)
{
	m_Fonts[name] = dae::ResourceManager::GetInstance().LoadFont(path, fontSize);
}

std::weak_ptr<dae::Font> FontManager::GetFont(const std::string& name)
{
	return m_Fonts[name];
}
