#include "MiniginPCH.h"
#include "SDL.h"
#include "SDL_ttf.h"
#include "Text.h"
#include "Transform.h"
#include "Texture2D.h"
#include "Renderer.h"
#include "Font.h"
#include "FontManager.h"

void Text::Update()
{
	if (m_NeedsUpdate)
	{
		const SDL_Color color = { 255,255,255 }; // only white text is supported now
		const auto surf = TTF_RenderText_Blended(m_pFont.lock()->GetFont(), m_Text.c_str(), color);
		if (surf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		auto texture = SDL_CreateTextureFromSurface(dae::Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);
		m_pTexture = std::make_shared<dae::Texture2D>(texture);
		m_NeedsUpdate = false;
	}
}

void Text::Render() const
{
	if (m_pTexture != nullptr)
	{
		const auto pos = GetTransform().lock()->GetPosition();
		dae::Renderer::GetInstance().RenderTexture(*m_pTexture, pos.x, pos.y);
	}
}

// This implementation uses the "dirty flag" pattern
void Text::SetText(const std::string& text)
{
	m_Text = text;
	m_NeedsUpdate = true;
}

void Text::SetFont(const std::string& name)
{
	m_pFont = FontManager::GetInstance().GetFont(name);
	m_NeedsUpdate = true;
}

void Text::SetFont(std::weak_ptr<dae::Font> font)
{
	m_pFont = font;
	m_NeedsUpdate = true;
}
