#pragma once
#include "Singleton.h"

struct GridTile
{
	char openDirections{ 0 };
};

class GridManager : public dae::Singleton<GridManager>
{
public:

	void LoadNewGrid(GridTile* grid);
	bool IsValidMovement(const Vector2& position, Direction direction, bool canDig);

	std::pair<int,int> TileIndexFromPosition(const Vector2& position);
	const Vector2 PositionFromTileIndex(int x, int y);
	const Vector2 PositionFromTileIndex(int index);

	static const Vector2 g_TileDimensions;

	GridTile& GetTile(std::pair<int, int> index);

	bool IsTileOpenAnyDirection(GridTile& tile);
	bool IsTileOpenInDirection(Direction direction, GridTile& tile);

	void OpenTileInDirection(Direction direction, GridTile& tile);
	void OpenTileInAllDirections(GridTile& tile);
	void OpenTileInAllDirections(int index);

	int GetGridWidth() const;
	int GetGridHeight() const;

	~GridManager() = default;
private:

	friend class dae::Singleton<GridManager>;
	GridManager() = default;

	static const int m_GridWidth = 15;
	static const int m_GridHeight = 10;

	GridTile* m_GridTiles{};

};

inline bool IsOnGrid(const Vector3& position)
{
	if ((int)(abs(position.x) + 0.5f) % (int)GridManager::g_TileDimensions.x == 0 && (int)(abs(position.y) + 0.5f) % (int)GridManager::g_TileDimensions.y == 0)
		return true;
	return false;
}