#pragma once
#include "Collider.h"
class BoxCollider final : public Physics::Collider
{
public:

	void Update() {}
	void Render() const {}

	void SetWidth(float width)
	{
		Collider::SetWidth(width);
	}

	void SetHeight(float height)
	{
		Collider::SetHeight(height);
	}

	BoxCollider()
		: Collider{}
	{
		Collider::SetType(Physics::ColliderType::Rectangle);
	}

	BoxCollider(const Vector3& center, float width, float height)
		: Collider{}
	{
		Collider::SetPosition(center);
		Collider::SetType(Physics::ColliderType::Rectangle);
		SetWidth(width);
		SetHeight(height);
	}

	~BoxCollider() = default;
	BoxCollider(const BoxCollider& other) = delete;
	BoxCollider(BoxCollider&& other) = delete;
	BoxCollider& operator=(const BoxCollider& other) = delete;
	BoxCollider& operator=(BoxCollider&& other) = delete;

};

