#include "MiniginPCH.h"
#include "Collider.h"
#include "Rigidbody.h"
#include "GameObject.h"


const Physics::ColliderInfo& Physics::Collider::GetInfo() const
{
	return m_Info;
}

void Physics::Collider::SetPlatform(bool value)
{
	m_Info.isPlatform = value;
}

void Physics::Collider::SetPosition(const Vector3& center)
{
	m_Info.center = center;
}

void Physics::Collider::SetPosition(float x, float y, float z)
{
	m_Info.center = Vector3(x, y, z);
}

void Physics::Collider::SetTrigger(bool value)
{
	m_IsTrigger = value;
}

bool Physics::Collider::IsTrigger() const
{
	return m_IsTrigger;
}

void Physics::Collider::OnAdd()
{
	AddObserver(GetGameObject());
}

void Physics::Collider::CollisionHappened(std::weak_ptr<Collider> otherCollider, ColliderKey)
{
	if (m_IsTrigger)
		notifyTrigger(otherCollider);
}
