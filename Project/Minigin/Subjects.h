#pragma once
#include <vector>
#include <memory>
#include "Observers.h"

class CollisionSubject
{
public:
	void AddObserver(std::weak_ptr<CollisionObserver> pObserver)
	{
		if(pObserver.lock())
			m_pObservers.push_back(pObserver);
	}

protected:
	void notifyTrigger(std::weak_ptr<Physics::Collider> other)
	{
		for (std::weak_ptr<CollisionObserver> o : m_pObservers)
		{
			o.lock()->OnTriggerEnter(other);
		}
	}
private:
	std::vector<std::weak_ptr<CollisionObserver>> m_pObservers{};
};