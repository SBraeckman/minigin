#include "MiniginPCH.h"
#include "ScoreText.h"
#include "ScoreManager.h"
#include <sstream>
#include <iomanip>
#include "Text.h"

ScoreText::ScoreText()
	: dae::GameObject()
{
	auto text = std::make_shared<Text>();
	text->SetFont("arcade");
	AddComponent(text);
	m_pText = text;
	SetPosition(-120, 100 - 8);
	UpdateText();
}

void ScoreText::Update()
{
	UpdateText();
	GameObject::Update();
}

void ScoreText::UpdateText()
{
	std::stringstream scoreStream{};
	scoreStream.width(5);
	scoreStream << std::setfill('0');
	scoreStream << std::to_string(ScoreManager::GetInstance().lock()->GetScore());
	m_pText.lock()->SetText(scoreStream.str());
}
