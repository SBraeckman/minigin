#pragma once
#include "PickupObject.h"

class MoneyPile : public PickupObject
{
public:
	MoneyPile();

	virtual void OnTriggerEnter(std::weak_ptr<Physics::Collider> other) override;
};