#include "MiniginPCH.h"
#include "MoneyBag.h"
#include "GridManager.h"
#include "CharacterController.h"
#include "Rigidbody.h"
#include "MoneyPile.h"
#include "AnimatedSpriteRenderer.h"
#include "SceneManager.h"
#include "Collider.h"
#include "Minigin.h"
#include "LevelScene.h"
#include "Transform.h"

MoneyBag::MoneyBag()
	:Actor()
{
	auto rc = std::make_shared<AnimatedSpriteRenderer>();
	rc->SetTexture("moneybag");
	rc->SetVariables(18, 16, 0.1f, false);
	AddComponent(rc);
	SetSpeed(m_FallSpeed);
	SetTag("moneybag");
}

void MoneyBag::Update()
{
	dae::GameObject::Update();
	std::pair<int, int> tileIndex = GridManager::GetInstance().TileIndexFromPosition(GetTransform().lock()->GetPosition());
	switch (m_CurrentState)
	{
	case MoneyBagState::Idle:
		tileIndex.second -= 1;
		if (GridManager::GetInstance().IsTileOpenAnyDirection(GridManager::GetInstance().GetTile(tileIndex)) && IsOnGrid(GetTransform().lock()->GetPosition()))
		{
			m_CurrentState = MoneyBagState::GonnaFall;
			GetComponent<AnimatedSpriteRenderer>()->Play();
		}
		break;
	case MoneyBagState::Falling:
		if (GetComponent<Physics::Rigidbody>()->GetVelocity().y == 0)
		{
			m_FallTimer = m_FallTime;

			m_CurrentState = MoneyBagState::Idle;

			if (m_FallHeight - (int)GetTransform().lock()->GetPosition().y >= 1.9f * GridManager::g_TileDimensions.y)
			{
				Break();
			}

			GetTransform().lock()->SetPosition(GridManager::GetInstance().PositionFromTileIndex(tileIndex.first + tileIndex.second));
		}
		else
		{
			Move(Direction::Down);
		}
		break;
	case MoneyBagState::GonnaFall:
		if (m_FallTimer <= 0)
			Fall();
		else
			m_FallTimer -= dae::Minigin::g_DeltaTime;
		break;
	case MoneyBagState::Pushing:
		Move(GetCharControl().lock()->GetDirection());
		if (IsOnGrid(GetTransform().lock()->GetPosition()))
		{
			m_CurrentState = MoneyBagState::Idle;
			SetPosition(GetTransform().lock()->GetPosition());
			tileIndex.second -= 1;
			if (GridManager::GetInstance().IsTileOpenAnyDirection(GridManager::GetInstance().GetTile(tileIndex)) && IsOnGrid(GetTransform().lock()->GetPosition()))
			{
				Fall();
			}
			return;
		}
		break;
	case MoneyBagState::GonnaPush:
		m_CurrentState = MoneyBagState::Pushing;
	}
}

void MoneyBag::OnTriggerEnter(std::weak_ptr<Physics::Collider> other)
{
	auto otherObj = std::dynamic_pointer_cast<Actor>(other.lock()->GetGameObject().lock());

	if (!otherObj)
		return;

	switch (m_CurrentState)
	{
	case MoneyBagState::Idle:
		switch (otherObj->GetCharControl().lock()->GetDirection())
		{
		case Direction::Left:
			SetSpeed(200);
			GetCharControl().lock()->SetCanDig(true);
			Move(Direction::Left);
			GetCharControl().lock()->SetCanDig(false);
			SetSpeed(m_FallSpeed);
			m_CurrentState = MoneyBagState::GonnaPush;
			break;
		case Direction::Right:
			SetSpeed(200);
			GetCharControl().lock()->SetCanDig(true);
			Move(Direction::Right);
			GetCharControl().lock()->SetCanDig(false);
			SetSpeed(m_FallSpeed);
			m_CurrentState = MoneyBagState::GonnaPush;
			break;
		}
		break;
	}
}

void MoneyBag::LateUpdate()
{
}

void MoneyBag::Break()
{
	auto pile = std::make_shared<MoneyPile>();
	pile->SetPosition(GetTransform().lock()->GetPosition());
	dae::SceneManager::GetInstance().GetActiveScene().lock()->Add(pile);
	SetEnabled(false);
}

void MoneyBag::Fall()
{
	GetComponent<AnimatedSpriteRenderer>()->Stop();
	m_FallHeight = (int)GetTransform().lock()->GetPosition().y;
	GetCharControl().lock()->SetCanDig(true);
	Move(Direction::Down);
	GetCharControl().lock()->SetCanDig(false);
	m_CurrentState = MoneyBagState::Falling;
}
