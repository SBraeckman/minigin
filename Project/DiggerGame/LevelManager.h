#pragma once
#include "SceneObject.h"

class Emerald;
class DiggerMobile;

namespace dae
{
	class Scene;
	class GameObject;
}

class LevelManager : public dae::SceneObject
{
public:

	LevelManager(const std::string& sceneName);

	virtual void Update() override;
	virtual void Render() const override {}
	virtual void LateUpdate() override {}

	void SetSceneName(const std::string& name);
	void GetAllEmeralds(std::weak_ptr<dae::Scene> scene);
	void SetPlayer(std::weak_ptr<DiggerMobile> player);

	virtual ~LevelManager() = default;
	LevelManager(const LevelManager& other) = delete;
	LevelManager(LevelManager&& other) = delete;
	LevelManager& operator=(const LevelManager& other) = delete;
	LevelManager& operator=(LevelManager&& other) = delete;

private:

	std::vector<std::weak_ptr<Emerald>> m_pEmeralds{};
	std::string m_SceneName{};

	static int m_PlayerLives;

	const int m_MaxLives{ 5 };
	const float m_playerRespawnTime{ 1.5f };
	float m_playerRespawnTimer{m_playerRespawnTime};
	bool m_hasToInitialise{false};

	
	std::weak_ptr<DiggerMobile> m_pPlayer{};
	std::vector<std::weak_ptr<dae::GameObject>> m_pLivesIcons{};

	bool CheckEmeraldWin();

	

};