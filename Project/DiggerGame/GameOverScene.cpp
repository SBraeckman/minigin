#include "MiniginPCH.h"
#include "GameOverScene.h"
#include <SDL.h>
#include "ScoreText.h"
#include "Text.h"
#include <sstream>
#include <iomanip>
#include "HighScores.h"
#include "ScoreManager.h"
#include "SceneManager.h"

GameOverScene::GameOverScene(const std::string& name)
	:Scene(name)
{
	auto obj = std::make_shared<dae::GameObject>();
	auto text = std::make_shared<Text>();

	obj->SetPosition(0, 0);
	obj->AddComponent(text);
	text->SetFont("arcade");
	text->SetText("___");
	Add(obj);
	m_pInitialsText = text;
	
	obj = std::make_shared<dae::GameObject>();
	text = std::make_shared<Text>();

	obj->SetPosition(0, 50);
	obj->AddComponent(text);
	text->SetFont("arcade");
	text->SetText("GAME OVER");
	Add(obj);

	obj = std::make_shared<ScoreText>();
	Add(obj);
}

void GameOverScene::Update(float e)
{
	HandleInput();
	Scene::Update(e);

	std::stringstream initialsDisplay{};

	initialsDisplay.width(3);
	initialsDisplay << std::setfill('_');
	initialsDisplay << std::left << m_Initials;

	m_pInitialsText.lock()->SetText(initialsDisplay.str());

	if (m_Initials.size() >= 3)
	{
		HighScores::GetInstance().AddScore(m_Initials, ScoreManager::GetInstance().lock()->GetScore());
		m_Initials = "";
		ScoreManager::GetInstance().lock()->Reset();
		dae::SceneManager::GetInstance().SetActiveScene("Menu");
	}
}

void GameOverScene::HandleInput()
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_KEYUP) {
			if (e.key.keysym.scancode >= SDL_SCANCODE_A && e.key.keysym.scancode <= SDL_SCANCODE_Z)
			{
				m_Initials += (char(e.key.keysym.scancode + 'A' - 4));
			}
		}
	}
}
