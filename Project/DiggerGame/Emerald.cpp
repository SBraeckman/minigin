#include "MiniginPCH.h"
#include "Emerald.h"
#include "SpriteRenderer.h"
#include "Collider.h"

Emerald::Emerald()
{
	auto rc = std::make_shared<SpriteRenderer>();
	rc->SetTexture("emerald");
	AddComponent(rc);
	SetTag("emerald");
	AddObserver(ScoreManager::GetInstance());
}

void Emerald::OnTriggerEnter(std::weak_ptr<Physics::Collider> other)
{
	if(other.lock()->GetGameObject().lock()->GetTag() == "diggermobile")
		NotifyScoreEvent(ScoreEvent::PickupEmerald);
	SetEnabled(false);
}
