#pragma once
#include "Actor.h"

class MoneyBag : public Actor
{
public:

	enum class MoneyBagState
	{
		Idle,
		Pushing,
		GonnaFall,
		Falling,
		GonnaPush
	};

	MoneyBag();

	virtual void Update() override;
	virtual void OnTriggerEnter(std::weak_ptr<Physics::Collider> other) override;
	virtual void LateUpdate() override;

private:

	int m_FallSpeed{60};
	int m_FallHeight{};
	MoneyBagState m_CurrentState{};

	const float m_FallTime{1}; //Time it always takes to fall
	float m_FallTimer{ 1 }; //Timer used to fall

	void Break();
	void Fall();
};