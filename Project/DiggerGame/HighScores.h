#pragma once
#include "Singleton.h"

using Score = std::pair<std::string, int>;

class HighScores : public dae::Singleton<HighScores>
{
public:

	static const int g_NrScores{ 10 };
	const std::vector<Score>& GetScores() const;
	virtual ~HighScores();

	void AddScore(const std::string& initials, int score);

private:

	friend class dae::Singleton<HighScores>;
	HighScores();
	std::vector<Score> m_Scores{};

	void SortScores();

};