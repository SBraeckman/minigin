#pragma once
#include "Scene.h"

struct GridTile;

class LevelScene : public dae::Scene
{
public:

	LevelScene(const std::string& name);
	virtual void OnActivate() override;
	virtual void Update(float elapsedTime) override;

	int GetLevelNumber();

	GridTile* GetGrid() const;

	virtual ~LevelScene();
	LevelScene(const LevelScene& other) = delete;
	LevelScene(LevelScene&& other) = delete;
	LevelScene& operator=(const LevelScene& other) = delete;
	LevelScene& operator=(LevelScene&& other) = delete;

	static int GetLevelsAmount();

private:

	static int m_LevelsAmount;

	float m_LevelStartTimer{ 1.f };
	int m_LevelIndex{};
	GridTile* m_pGrid{};
};