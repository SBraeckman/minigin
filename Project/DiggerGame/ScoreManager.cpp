#include "MiniginPCH.h"
#include "ScoreManager.h"

std::shared_ptr<ScoreManager> ScoreManager::m_pScoreManager;

void ScoreManager::Notify(ScoreEvent event)
{
	switch (event)
	{
	case ScoreEvent::PickupEmerald:
		IncreaseScore(25);
		break;
	case ScoreEvent::KillNobbin:
		IncreaseScore(250);
		break;
	case ScoreEvent::KillHobbin:
		IncreaseScore(250);
		break;
	case ScoreEvent::PickupGold:
		IncreaseScore(500);
		break;
	case ScoreEvent::EmeraldStreak:
		IncreaseScore(250);
		break;
	}
}

int ScoreManager::GetScore() const
{
	return m_Score;
}

void ScoreManager::Reset()
{
	m_Score = 0;
	m_ScoreForLifeCounter = 20000;
	m_livesBank = 0;
}

int ScoreManager::GetLives()
{
	int lives = m_livesBank;
	m_livesBank = 0;
	return lives;
}

std::weak_ptr<ScoreManager> ScoreManager::GetInstance()
{
	if (!m_pScoreManager)
	{
		m_pScoreManager = std::make_shared<ScoreManager>(ScoreManager());
	}
	return m_pScoreManager;
}

void ScoreManager::IncreaseScore(int amount)
{
	m_Score += amount;
	if (m_ScoreForLifeCounter - m_Score < 0)
	{
		m_ScoreForLifeCounter += 20000;
		m_livesBank++;
	}
}
