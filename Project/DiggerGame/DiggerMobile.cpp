#include "MiniginPCH.h"
#include "DiggerMobile.h"
#include "AnimatedSpriteRenderer.h"
#include "BoxCollider.h"
#include "Rigidbody.h"
#include "CharacterController.h"
#include "Minigin.h"

DiggerMobile::DiggerMobile()
	:Actor()
{
	auto spriteRenderer = std::make_shared<AnimatedSpriteRenderer>();
	spriteRenderer->SetTexture("digger");
	spriteRenderer->SetVariables(16, 16, 0.1f, true);
	AddComponent(spriteRenderer);
	m_pSpriteRenderer = spriteRenderer;
	std::weak_ptr<Physics::Rigidbody> rigidbody = GetComponent<Physics::Rigidbody>();
	std::weak_ptr<CharacterController> cc = GetComponent<CharacterController>();
	cc.lock()->SetCanDig(true);
	SetSpeed(60);
	SetTag("diggermobile");
}

void DiggerMobile::OnTriggerEnter(std::weak_ptr<Physics::Collider> other)
{
	if (other.lock()->GetGameObject().lock()->GetTag() == "moneybag")
	{
		if (other.lock()->GetGameObject().lock()->GetComponent<Physics::Rigidbody>()->GetVelocity().y < 0 &&
			other.lock()->GetGameObject().lock()->GetTransform().lock()->GetPosition().y - 1 > GetTransform().lock()->GetPosition().y)
		{
			StartDying();
		}
		else
			SetPosition(GetTransform().lock()->GetPosition());
	}
}

void DiggerMobile::Update()
{
	GameObject::Update();
	auto direction = GetCharControl().lock()->GetDirection();

	switch (direction)
	{
	case Direction::Right:
		m_pSpriteRenderer.lock()->SetRowNr(0);
		break;
	case Direction::Left:
		m_pSpriteRenderer.lock()->SetRowNr(1);
		break;
	case Direction::Up:
		m_pSpriteRenderer.lock()->SetRowNr(2);
		break;
	case Direction::Down:
		m_pSpriteRenderer.lock()->SetRowNr(3);
		break;
	}
}

bool DiggerMobile::IsDying()
{
	return m_IsDying;
}

void DiggerMobile::StartDying()
{
	m_IsDying = true;
	SetEnabled(false);
}

void DiggerMobile::Die()
{
	SetEnabled(true);
	m_IsDying = false;
	Reset();
}

void DiggerMobile::Reset()
{
	SetPosition(m_startPosition);
}

