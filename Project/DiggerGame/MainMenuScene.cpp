#include "MiniginPCH.h"
#include "MainMenuScene.h"
#include "HighScores.h"
#include "Text.h"
#include "GameObject.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "LevelScene.h"
#include "LevelParser.h"
#include <sstream>
#include <iomanip>

MainMenuScene::MainMenuScene(const std::string& name)
	:Scene(name)
{
	auto obj = std::make_shared<dae::GameObject>();
	Vector2 objPos{ -50, 100 - 40 };
	obj->SetPosition(objPos);
	auto text = std::make_shared<Text>();
	text->SetFont("arcade");
	text->SetText("HIGH SCORES");
	obj->AddComponent(text);
	Add(obj);
	objPos.y -= 12;
	auto scores = HighScores::GetInstance().GetScores();
	for (int i{}; i < HighScores::g_NrScores; i++)
	{
		std::stringstream scoreString{};
		obj = std::make_shared<dae::GameObject>();
		text = std::make_shared<Text>();
		m_pScoreTexts.push_back(text);
		scoreString << scores[i].first;
		scoreString.width(8);
		scoreString << std::right << scores[i].second;
		text->SetFont("arcade");
		text->SetText(scoreString.str());
		obj->AddComponent(text);
		obj->SetPosition(objPos);
		Add(obj);
		objPos.y -= 11;
	}
}

void MainMenuScene::OnActivate()
{
	auto& sceneManager = dae::SceneManager::GetInstance();
	std::shared_ptr<dae::Scene> scene{};
	scene = std::make_shared<LevelScene>("Level0");
	sceneManager.AddScene("Level0", scene);
	scene = std::make_shared<LevelScene>("Level1");
	sceneManager.AddScene("Level1", scene);
	scene = std::make_shared<LevelScene>("Level2");
	sceneManager.AddScene("Level2", scene);

	LevelParser::LoadLevelIntoScene(sceneManager.GetScene("Level0"), "Level1.txt");
	LevelParser::LoadLevelIntoScene(sceneManager.GetScene("Level1"), "Level2.txt");
	LevelParser::LoadLevelIntoScene(sceneManager.GetScene("Level2"), "Level3.txt");

	ReloadScores();
}

void MainMenuScene::HandleInputs()
{
	auto command = dae::InputManager::GetInstance().HandleInput();
	if (std::dynamic_pointer_cast<FireCommand>(command))
	{
		dae::SceneManager::GetInstance().SetActiveScene("Level0");
	}
}

void MainMenuScene::ReloadScores()
{
	auto scores = HighScores::GetInstance().GetScores();
	for (int i{}; i < HighScores::g_NrScores; i++)
	{
		std::stringstream scoreString{};
		scoreString << scores[i].first;
		scoreString.width(8);
		scoreString << std::right << scores[i].second;
		m_pScoreTexts[i].lock()->SetText(scoreString.str());
	}
}
