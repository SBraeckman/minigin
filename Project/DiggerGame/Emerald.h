#pragma once
#include "PickupObject.h"

class Emerald : public PickupObject
{
public:
	Emerald();

	virtual void OnTriggerEnter(std::weak_ptr<Physics::Collider> other) override;
};