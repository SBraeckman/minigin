#pragma once

namespace dae
{
	class Scene;
}

namespace LevelParser
{
	
	void LoadLevelIntoScene(std::weak_ptr<dae::Scene> scene, const std::string& levelFile);

}