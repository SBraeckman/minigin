#pragma once
#include <Scene.h>

class Text;

class MainMenuScene : public dae::Scene
{
public:

	MainMenuScene(const std::string& name);
	virtual void OnActivate() override;

	virtual ~MainMenuScene() = default;
	MainMenuScene(const MainMenuScene& other) = delete;
	MainMenuScene(MainMenuScene&& other) = delete;
	MainMenuScene& operator=(const MainMenuScene& other) = delete;
	MainMenuScene& operator=(MainMenuScene&& other) = delete;

protected:
	virtual void HandleInputs() override;

private:

	void ReloadScores();

	std::vector<std::weak_ptr<Text>> m_pScoreTexts{};

};

