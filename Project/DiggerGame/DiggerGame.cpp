#include "MiniginPCH.h"
#include "DiggerGame.h"
#include <SDL.h>
#include "SceneManager.h"
#include "TextureManager.h"
#include "LevelScene.h"
#include "GridManager.h"
#include "FontManager.h"
#include "MainMenuScene.h"
#include "GameOverScene.h"

void DiggerGame::LoadGame() const
{
	TextureManager::GetInstance().AddTexture("Sprites/MoneyBag.png", "moneybag");
	TextureManager::GetInstance().AddTexture("Sprites/MoneyPile.png", "moneypile");
	TextureManager::GetInstance().AddTexture("Sprites/Emerald.png", "emerald");
	TextureManager::GetInstance().AddTexture("Sprites/Digger.png", "digger");
	FontManager::GetInstance().AddFont("joystix_monospace.ttf", "arcade", 14);
	dae::SceneManager& sceneManager = dae::SceneManager::GetInstance();

	std::shared_ptr<dae::Scene> scene{};
	scene = std::make_shared<MainMenuScene>("main");
	sceneManager.AddScene("Menu", scene);
	scene = std::make_shared<GameOverScene>("Gameover");
	sceneManager.AddScene("Gameover", scene);
	sceneManager.SetActiveScene("Menu");

	GridManager::GetInstance().LoadNewGrid(std::static_pointer_cast<LevelScene>(sceneManager.GetActiveScene().lock())->GetGrid());
}
