#include "MiniginPCH.h"
#include "LevelManager.h"
#include "LevelScene.h"
#include "Emerald.h"
#include "SceneManager.h"
#include "DiggerMobile.h"
#include "AnimatedSpriteRenderer.h"
#include "Minigin.h"

int LevelManager::m_PlayerLives{ 3 };

LevelManager::LevelManager(const std::string& sceneName)
	: dae::SceneObject()
	, m_SceneName{sceneName}
{
	m_hasToInitialise = true;
}

void LevelManager::Update()
{
	if (m_hasToInitialise)
	{
		std::shared_ptr<dae::GameObject> obj{};
		std::shared_ptr<AnimatedSpriteRenderer> rc{};
		Vector3 objPos{ -80, 100 - 7, 0 };

		for (int i{}; i < m_MaxLives; i++)
		{
			obj = std::make_shared<dae::GameObject>();
			obj->SetPosition(objPos);
			objPos.x += 18;
			rc = std::make_shared<AnimatedSpriteRenderer>();
			rc->SetVariables(16, 16, 0.2f, false);
			rc->SetTexture("digger");
			obj->AddComponent(rc);
			m_pLivesIcons.push_back(obj);
			dae::SceneManager::GetInstance().GetScene(m_SceneName).lock()->Add(obj);
		}
		m_hasToInitialise = false;
	}

	if (m_pPlayer.lock()->IsDying())
	{
		m_playerRespawnTimer -= dae::Minigin::g_DeltaTime;
		if (m_playerRespawnTimer <= 0)
		{
			m_PlayerLives--;
			m_playerRespawnTimer = m_playerRespawnTime;
			m_pPlayer.lock()->Die();
		}
	}

	if (m_PlayerLives <= 0)
	{
		m_PlayerLives = 3;
		dae::SceneManager::GetInstance().SetActiveScene("Gameover");
	}

	for (int i{}; i < m_MaxLives; i++)
	{
		if (i < m_PlayerLives - 1)
			m_pLivesIcons[i].lock()->SetEnabled(true);
		else
			m_pLivesIcons[i].lock()->SetEnabled(false);
	}

	m_PlayerLives += ScoreManager::GetInstance().lock()->GetLives();

	if (CheckEmeraldWin())
	{
		int levelIndex = std::static_pointer_cast<LevelScene>(dae::SceneManager::GetInstance().GetScene(m_SceneName).lock())->GetLevelNumber();
		levelIndex++;
		if (levelIndex != LevelScene::GetLevelsAmount())
			dae::SceneManager::GetInstance().SetActiveScene("Level" + std::to_string(levelIndex));
		else
			dae::SceneManager::GetInstance().SetActiveScene("Gameover");
	}
}

void LevelManager::SetSceneName(const std::string& name)
{
	m_SceneName = name;
}

void LevelManager::GetAllEmeralds(std::weak_ptr<dae::Scene> scene)
{
	const std::shared_ptr<std::vector<std::shared_ptr<Emerald>>> sceneEmeralds = scene.lock()->GetObjects<Emerald>();
	for (const auto e : *sceneEmeralds)
	{
		m_pEmeralds.push_back(e);
	}
}

void LevelManager::SetPlayer(std::weak_ptr<DiggerMobile> player)
{
	m_pPlayer = player;
}

bool LevelManager::CheckEmeraldWin()
{
	for (auto e : m_pEmeralds)
	{
		if (e.lock()->IsEnabled())
			return false;
	}
	return true;
}
