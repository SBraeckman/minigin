#pragma once
#include "GameObject.h"
#include "ScoreManager.h"

class PickupObject : public dae::GameObject, public ScoreSubject
{
public:

	PickupObject();
	virtual ~PickupObject() = default;

};

