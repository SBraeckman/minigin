#pragma once
#include "GameObject.h"

namespace dae
{
	class Texture2D;
}

class DirtBG : public dae::GameObject
{
public:

	DirtBG();
	virtual ~DirtBG();

	virtual void Update() override;
	void SetPlayer(std::weak_ptr<dae::GameObject> p);
	void DigTile(const Vector2& position);

	DirtBG(const DirtBG& other) = delete;
	DirtBG(DirtBG&& other) = delete;
	DirtBG& operator=(const DirtBG& other) = delete;
	DirtBG& operator=(DirtBG&& other) = delete;

private:

	std::weak_ptr<dae::GameObject> m_pPlayer{};
	std::shared_ptr<dae::Texture2D> m_pDirtTexture{};

	uint32_t *m_pPixels{};
	int m_PixelAmount{};
};

