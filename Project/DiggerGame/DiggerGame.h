#pragma once
#include "Minigin.h"

class DiggerGame : public dae::Minigin
{
public:
	DiggerGame() = default;
	virtual ~DiggerGame() = default;
	DiggerGame(const DiggerGame& other) = delete;
	DiggerGame(DiggerGame&& other) = delete;
	DiggerGame& operator=(const DiggerGame& other) = delete;
	DiggerGame& operator=(DiggerGame&& other) = delete;

	virtual void LoadGame() const override;
};

