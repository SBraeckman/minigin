#pragma once
#include "GameObject.h"

class Text;

class ScoreText : public dae::GameObject
{
public:

	ScoreText();

	virtual void Update() override;

private:

	std::weak_ptr<Text> m_pText;

	void UpdateText();

};