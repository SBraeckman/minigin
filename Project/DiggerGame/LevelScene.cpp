#include "MiniginPCH.h"
#include "LevelScene.h"
#include "GridManager.h"
#include "LevelManager.h"

int LevelScene::m_LevelsAmount{0};

LevelScene::LevelScene(const std::string& name)
	:Scene(name)
{
	m_LevelIndex = m_LevelsAmount;
	m_LevelsAmount++;
	m_pGrid = new GridTile[GridManager::GetInstance().GetGridWidth() * GridManager::GetInstance().GetGridHeight()];
	auto lm = std::make_shared<LevelManager>(name);
	Add(lm);
}

void LevelScene::OnActivate()
{
	GridManager::GetInstance().LoadNewGrid(GetGrid());
}

void LevelScene::Update(float elapsedTime)
{
	if (m_LevelStartTimer > 0)
	{
		m_LevelStartTimer -= elapsedTime;
	}
	else
	{
		Scene::Update(elapsedTime);
	}
}

int LevelScene::GetLevelNumber()
{
	return m_LevelIndex;
}

GridTile* LevelScene::GetGrid() const
{
	return m_pGrid;
}

LevelScene::~LevelScene()
{
	delete[] m_pGrid;
}

int LevelScene::GetLevelsAmount()
{
	return m_LevelsAmount;
}
