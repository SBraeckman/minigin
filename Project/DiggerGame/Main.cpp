#include "MiniginPCH.h"

#if _DEBUG
// ReSharper disable once CppUnusedIncludeDirective
#include <vld.h>
#endif

#include "DiggerGame.h"

int main(int, char* []) {
	DiggerGame game;
	game.Run();
	return 0;
}