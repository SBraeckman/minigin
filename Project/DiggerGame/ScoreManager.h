#pragma once

enum class ScoreEvent
{
	PickupEmerald,
	PickupGold,
	KillNobbin,
	KillHobbin,
	EmeraldStreak
};

class ScoreObserver
{
public:
	virtual ~ScoreObserver() = default;
	virtual void Notify(ScoreEvent event) = 0;
};

class ScoreSubject
{
public:
	void AddObserver(std::weak_ptr<ScoreObserver> o)
	{
		m_pObservers.push_back(o);
	}
protected:
	void NotifyScoreEvent(ScoreEvent event)
	{
		for (std::weak_ptr<ScoreObserver> o : m_pObservers)
		{
			o.lock()->Notify(event);
		}
	}
private:
	std::vector<std::weak_ptr<ScoreObserver>> m_pObservers;
};

class ScoreManager : public ScoreObserver
{
public:

	virtual void Notify(ScoreEvent event);
	int GetScore() const;
	void Reset();
	int GetLives();
	static std::weak_ptr<ScoreManager> GetInstance();

	~ScoreManager() = default;
	ScoreManager(const ScoreManager& other) = delete;
	ScoreManager(ScoreManager&& other) = default;
	ScoreManager& operator=(const ScoreManager& other) = delete;
	ScoreManager& operator=(ScoreManager&& other) = delete;

private:

	static std::shared_ptr<ScoreManager> m_pScoreManager;
	int m_Score{};
	int m_ScoreForLifeCounter{20000};
	int m_livesBank{};

	ScoreManager() = default;

	void IncreaseScore(int amount);

};

