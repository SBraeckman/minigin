#include "MiniginPCH.h"
#include "HighScores.h"
#include <algorithm>
#include <fstream>
#include <regex>

const std::vector<Score>& HighScores::GetScores() const
{
	return m_Scores;
}

HighScores::~HighScores()
{
	std::ofstream scoreFile{};
	scoreFile.open("../Data/Scores.txt");
	for (Score s : m_Scores)
	{
		scoreFile << s.first << s.second << '\n';
	}
}

void HighScores::AddScore(const std::string& initials, int score)
{
	m_Scores.push_back(std::make_pair(initials, score));

	SortScores();

	if(m_Scores.size() > 10)
		m_Scores.pop_back();
}

HighScores::HighScores()
{
	std::ifstream scoreFile{};
	std::regex scoreRegex{"(\\D+)(\\d+)"};
	std::smatch matches{};
	std::string line{};
	scoreFile.open("../Data/Scores.txt");

	for (int i{}; i < 10; i++)
	{
		AddScore("...", 0);
	}

	while (std::getline(scoreFile, line))
	{
		if (std::regex_search(line, matches, scoreRegex))
		{
			AddScore(matches[1], std::stoi(matches[2]));
		}
	}
}

void HighScores::SortScores()
{
	auto sortFunction = [](Score first, Score second)
	{
		return first.second > second.second;
	};

	std::sort(m_Scores.begin(), m_Scores.end(), sortFunction);
	
}
