#include "MiniginPCH.h"
#include "PickupObject.h"
#include "BoxCollider.h"
#include "Rigidbody.h"

PickupObject::PickupObject()
{
	auto boxCollider = std::make_shared<BoxCollider>();
	boxCollider->SetWidth(12);
	boxCollider->SetHeight(12);
	boxCollider->SetPosition(Vector3(0, 0, 0));
	boxCollider->SetTrigger(true);
	AddComponent(boxCollider);

	auto Nrigidbody = std::make_shared<Physics::Rigidbody>();
	Nrigidbody->SetType(Physics::RigidbodyType::Static);
	Nrigidbody->AddCollider(boxCollider);
	AddComponent(Nrigidbody);
}
