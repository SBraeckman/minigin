#pragma once
#include <Scene.h>

class Text;
class GameOverScene : public dae::Scene
{
public:

	GameOverScene(const std::string& name);

	virtual void Update(float elapsedTime);

	virtual ~GameOverScene() = default;
	GameOverScene(const GameOverScene& other) = delete;
	GameOverScene(GameOverScene&& other) = delete;
	GameOverScene& operator=(const GameOverScene& other) = delete;
	GameOverScene& operator=(GameOverScene&& other) = delete;

protected:

	virtual void HandleInput();

private:

	std::string m_Initials{};
	std::weak_ptr<Text> m_pInitialsText;
};

