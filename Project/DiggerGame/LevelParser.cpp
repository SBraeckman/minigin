#include "MiniginPCH.h"
#include "LevelParser.h"
#include "GridManager.h"
#include "Scene.h"
#include "MoneyBag.h"
#include "Emerald.h"
#include "DirtBG.h"
#include "DiggerMobile.h"
#include "LevelScene.h"
#include "LevelManager.h"
#include <fstream>
#include "ScoreText.h"

void LevelParser::LoadLevelIntoScene(std::weak_ptr<dae::Scene> scene, const std::string& filename)
{
	GridManager::GetInstance().LoadNewGrid(std::static_pointer_cast<LevelScene>(scene.lock())->GetGrid());

#pragma region make Background

	auto bgo = std::make_shared<DirtBG>();
	scene.lock()->Add(bgo);

#pragma endregion

	std::ifstream file{};
	file.open("../Data/Levels/" + filename);
	char c{};
	std::pair<int, int> index{0,9};
	std::shared_ptr<dae::GameObject> obj{};
	while (file.get(c))
	{
		switch (c)
		{
		case 'C':
			index.first++;
			break;
		case 'P':
			obj = std::make_shared<DiggerMobile>();
			scene.lock()->Add(obj);
			bgo->SetPlayer(obj);
			scene.lock()->SetControlledActor(std::static_pointer_cast<Actor>(obj));
			obj->SetPosition(GridManager::GetInstance().PositionFromTileIndex(index.first, index.second));
			std::static_pointer_cast<Actor>(obj)->SetStartPosition(GridManager::GetInstance().PositionFromTileIndex(index.first, index.second));
			scene.lock()->GetObjectOfType<LevelManager>()->SetPlayer(std::static_pointer_cast<DiggerMobile>(obj));
		case 'O':
			bgo->DigTile(GridManager::GetInstance().PositionFromTileIndex(index.first, index.second));
			GridManager::GetInstance().OpenTileInAllDirections(index.first + (index.second * GridManager::GetInstance().GetGridWidth()));
			index.first++;
			break;
		case 'E':
			obj = std::make_shared<Emerald>();
			obj->SetPosition(GridManager::GetInstance().PositionFromTileIndex(index.first, index.second));
			scene.lock()->Add(obj);
			index.first++;
			break;
		case 'M':
			obj = std::make_shared<MoneyBag>();
			obj->SetPosition(GridManager::GetInstance().PositionFromTileIndex(index.first, index.second));
			scene.lock()->Add(obj);
			index.first++;
			break;
		default:
			break;
		}

		if (index.first > 14)
		{
			index.first = 0;
			index.second--;
		}
	}

	scene.lock()->GetObjectOfType<LevelManager>()->GetAllEmeralds(scene);

	obj = std::make_shared<ScoreText>();
	scene.lock()->Add(obj);
}
