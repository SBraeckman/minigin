#include "MiniginPCH.h"
#include "MoneyPile.h"
#include "SpriteRenderer.h"
#include "Collider.h"

MoneyPile::MoneyPile()
	:PickupObject()
{
	auto rc = std::make_shared<SpriteRenderer>();
	rc->SetTexture("moneypile");
	AddComponent(rc);
	SetTag("moneypile");
	AddObserver(ScoreManager::GetInstance());
}

void MoneyPile::OnTriggerEnter(std::weak_ptr<Physics::Collider> other)
{
	if (other.lock()->GetGameObject().lock()->GetTag() == "diggermobile")
		NotifyScoreEvent(ScoreEvent::PickupGold);
	SetEnabled(false);
}
