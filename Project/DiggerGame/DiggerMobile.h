#pragma once
#include "Actor.h"

class AnimatedSpriteRenderer;

class DiggerMobile : public Actor
{
public:

	DiggerMobile();
	virtual void OnTriggerEnter(std::weak_ptr<Physics::Collider> other);

	virtual void Update() override;

	bool IsDying();
	void StartDying();
	void Die();
	void Reset();

	virtual ~DiggerMobile() = default;
	DiggerMobile(const DiggerMobile& other) = delete;
	DiggerMobile(DiggerMobile&& other) = delete;
	DiggerMobile& operator=(const DiggerMobile& other) = delete;
	DiggerMobile& operator=(DiggerMobile&& other) = delete;

private:

	std::weak_ptr<AnimatedSpriteRenderer> m_pSpriteRenderer;

	bool m_IsDying{};

};