#include "MiniginPCH.h"
#include "DirtBG.h"
#include "SDL.h"
#include "Renderer.h"
#include "Minigin.h"
#include "SpriteRenderer.h"
#include "GridManager.h"
#include "Texture2D.h"

DirtBG::DirtBG() : dae::GameObject()
{
	SetPosition(0, -8);

	SDL_Texture* pdirtTex = SDL_CreateTexture(
		  dae::Renderer::GetInstance().GetSDLRenderer()
		, SDL_PIXELFORMAT_ARGB8888
		, SDL_TEXTUREACCESS_STATIC
		, (int)dae::Minigin::g_WindowSize.x / 3
		, (int)(dae::Minigin::g_WindowSize.y / 3) - 16);

	m_pDirtTexture = std::make_shared<dae::Texture2D>(pdirtTex);
	auto rc = std::make_shared<SpriteRenderer>();
	AddComponent(rc);
	rc->SetTexture(m_pDirtTexture);

	m_PixelAmount = (int)((dae::Minigin::g_WindowSize.x / 3) * (dae::Minigin::g_WindowSize.y / 3));

	m_pPixels = new Uint32[m_PixelAmount];
	for (int i{}; i < m_PixelAmount; i++)
	{
		//Horribly gross trial and error math equation that generates the background pattern
		int heightModifier = (int)(i / (int)(dae::Minigin::g_WindowSize.x / 3)) % 4;
		int colorIndex = i % 10;
		if ((colorIndex < 2 + heightModifier && colorIndex + 1 > heightModifier)  || (colorIndex > 7 - heightModifier && colorIndex < 10 - heightModifier))
		{
			m_pPixels[i] = 0xCC0000;
		}
		else
		{
			m_pPixels[i] = 0xCC7400;
		}
	}
	SDL_UpdateTexture(m_pDirtTexture->GetSDLTexture(), NULL, m_pPixels, (int)dae::Minigin::g_WindowSize.x / 3 * sizeof(Uint32));
}

DirtBG::~DirtBG()
{
	delete[] m_pPixels;
}

void DirtBG::Update()
{
	if (!m_pPlayer.lock()->IsEnabled())
		return;

	float digDistance = 7.5f;
	Vector3 playerPos = m_pPlayer.lock()->GetTransform().lock()->GetPosition();
	Vector2 pixelPos = Vector2();
	for (int i{}; i < m_PixelAmount; i++)
	{
		pixelPos.x = (float)(i % (int)((dae::Minigin::g_WindowSize.x) / 3)) - 160;
		pixelPos.y = (float)(i / (int)((dae::Minigin::g_WindowSize.x) / 3)) - 84;

		float distance = std::sqrt(pow(pixelPos.x - playerPos.x, 2) + pow(pixelPos.y + playerPos.y, 2));

		if (distance < digDistance)
		{
			m_pPixels[i] = 0;
		}
	}
	SDL_UpdateTexture(m_pDirtTexture->GetSDLTexture(), NULL, m_pPixels , (int)dae::Minigin::g_WindowSize.x / 3 * sizeof(Uint32));
}

void DirtBG::SetPlayer(std::weak_ptr<dae::GameObject> p)
{
	m_pPlayer = p;
}

void DirtBG::DigTile(const Vector2& position)
{
	Vector2 pixelPos = Vector2();
	for (int i{}; i < m_PixelAmount; i++)
	{
		pixelPos.x = (float)(i % (int)((dae::Minigin::g_WindowSize.x) / 3)) - 160;
		pixelPos.y = (float)(i / (int)((dae::Minigin::g_WindowSize.x) / 3)) - 84;

		if (pixelPos.x < (position.x + GridManager::g_TileDimensions.x / 2) + 1 && pixelPos.x > (position.x - GridManager::g_TileDimensions.x / 2) - 1 &&
			pixelPos.y < (-position.y + GridManager::g_TileDimensions.y / 2) && pixelPos.y > (-position.y - GridManager::g_TileDimensions.y / 2) - 1)
			m_pPixels[i] = 0;
	}
	SDL_UpdateTexture(m_pDirtTexture->GetSDLTexture(), NULL, m_pPixels, (int)dae::Minigin::g_WindowSize.x / 3 * sizeof(Uint32));
}
